﻿using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor.SceneManagement;
[ExecuteAlways]
public class TileEditor : EditorWindow
{
	static ScriptableObject so;

	TileController[] tiles;

	public static TileEditor instance;

	static float tileSizes = 4.0f;
	static float tileHeight = 3.0f;
	static float tileOffset = 0.0f;
	static bool showTileGUI = true;

	static float linkSize = 50.0f;

	static bool isLinkMode = false;
	static TileController selectedTileController;

	static ElementList soElementList;
	
	static bool onlyDecorations = false;
	static int decorationPasses = 1;

	[MenuItem("SpaceCrawler/TileEditor")]
	static void Init()
	{
		if(instance == null) instance = GetWindow<TileEditor>();
		instance.minSize = new Vector2(350, 50);
		instance.Show();
	}

	private void OnEnable()
	{
		SceneView.duringSceneGui += DuringSceneGUI;
	}

	private void DuringSceneGUI(SceneView sceneView)
	{
		Camera cam = sceneView.camera;

		if (isLinkMode)
		{
			HandleUtility.AddDefaultControl(0);
			
			if (Event.current.type == EventType.MouseDrag && Event.current.button == 0 && Event.current.alt == false) //Quand on drag avec le click gauche
			{
				tiles = FindObjectsOfType<TileController>();

				Vector2 mousePos = Event.current.mousePosition;

				mousePos.y = cam.pixelHeight - mousePos.y;
				
				foreach (TileController tc in tiles)
				{
					Vector2 objPos = cam.WorldToScreenPoint(tc.transform.position);
					
					float d = Vector2.Distance(objPos, mousePos);
					
					if (d < linkSize) //si la distance entre l'objet et la souris est inférieure a 10.0f
					{
						if (selectedTileController == null) selectedTileController = tc; //Si aucune tile n'est selectionnée la tile actuelle est selectionée
						else if(selectedTileController != tc)
						{
							Vector3 selectedPos = cam.WorldToScreenPoint(selectedTileController.transform.position);

							if (d > Vector3.Distance(selectedPos, mousePos)) continue;
							else
							{
								TileController.Connect(tc, selectedTileController);
								selectedTileController = tc;
							}
						}
					}
					
				}
				Event.current.Use();
			}

			if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
			{
				selectedTileController = default;
			}

			if (Event.current.type == EventType.MouseDown && Event.current.button == 1)
			{
				tiles = FindObjectsOfType<TileController>();

				Vector2 mousePos = Event.current.mousePosition;

				mousePos.y = cam.pixelHeight - mousePos.y;

				foreach (TileController tc in tiles)
				{
					Vector2 objPos = cam.WorldToScreenPoint(tc.transform.position);

					float d = Vector2.Distance(objPos, mousePos);

					if (d < linkSize) //si la distance entre l'objet et la souris est inférieure a 10.0f
					{
						for (int i = tc.LinkedTiles.Count - 1; i >= 0; i--)
						{
							TileController.Disconnect(tc, tc.LinkedTiles[i]);
						}
					}

				}
				Event.current.Use();
			}
			sceneView.Repaint();
		}
	}

	private void OnGUI()
	{
		EditorGUILayout.BeginHorizontal();
		{
			EditorGUILayout.BeginVertical();
			{
				GUILayout.Label("Tile Settings : ");
				EditorGUI.indentLevel++;

				tileSizes = EditorGUILayout.Slider("Tile Size", tileSizes, 1.0f, 7.0f);
				tileHeight = EditorGUILayout.Slider("Tile Height", tileHeight, 1.0f, 5.0f);
				tileOffset = EditorGUILayout.Slider("Offset", tileOffset, 0f, 1f);

				GUILayout.Space(10);

				GUILayout.Label("Editor Settings : ");

				showTileGUI = EditorGUILayout.Toggle("Show Tiles GUI", showTileGUI);
				linkSize = EditorGUILayout.Slider("Link Size", linkSize, 1.0f, 1000.0f);

				GUILayout.Space(10);

				GUILayout.Label("Generation options : ");

				soElementList = EditorGUILayout.ObjectField("Element List", soElementList, typeof(ElementList), false) as ElementList;

				GUILayout.Space(10);

				GUILayout.Label("Tiles and links : ");

				if (GUILayout.Button("Select all tiles", GUILayout.MinHeight(30)))
				{
					Selection.objects = GetAllTileControllersObjects();
					SetDirtyScene();
				}

				//Bouton pour arrondir les positions des Tiles
				/*
				if (GUILayout.Button("Round Tile Pos", GUILayout.MinHeight(30)))
				{
					GameObject[] gos = GetAllTileControllersObjects();

					foreach (GameObject go in gos)
					{
						go.transform.position = new Vector3(Mathf.Round(go.transform.position.x), Mathf.Round(go.transform.position.y), Mathf.Round(go.transform.position.z));
					}
				}
				*/

				EditorGUILayout.BeginHorizontal();
				{

					GUI.color = isLinkMode ? Color.red : Color.white;

					if (GUILayout.Button("Edit Links Mode", GUILayout.MinHeight(30)))
					{
						isLinkMode = !isLinkMode;

						TileController.showLinks = isLinkMode;

						if (isLinkMode)
						{
							Tools.hidden = true;
							Selection.objects = GetAllTileControllersObjects();
						}
						else
						{
							Tools.hidden = false;
						}
					}

					GUI.color = Color.white;

					if (GUILayout.Button("Remove Selected Links", GUILayout.MinHeight(30)))
					{
						Object[] obs = Selection.objects;

						foreach (object o in obs)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc != null)
								tc.LinkedTiles.Clear();
						}
						SetDirtyScene();
					}

				}
				EditorGUILayout.EndHorizontal();

				if (GUILayout.Button("Remove All Solo Tiles", GUILayout.MinHeight(30)))
				{
					GameObject[] gos = GetAllTileControllersObjects();

					for (int i = gos.Length - 1; i >= 0; i--)
					{
						TileController tc = gos[i].GetComponent<TileController>();
						if (tc != null)
						{
							int links = tc.LinkedTiles.Count;
							if (links <= 0) DestroyImmediate(gos[i]);
						}
					}
					SetDirtyScene();
				}

				GUILayout.Space(10);

				GUILayout.Label("Generation and Tile Content :");

				EditorGUILayout.BeginHorizontal();
				{
					if (GUILayout.Button("Generate", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							go.GetComponent<TileController>()?.GenerateTile(soElementList);
						}
						SetDirtyScene();
					}

					else if (GUILayout.Button("Remove", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							go.GetComponent<TileController>()?.RemoveChildren(go.transform);
						}
						SetDirtyScene();
					}
				}
				EditorGUILayout.EndHorizontal();

				GUILayout.Space(10);

				GUILayout.Label("Specific Generation Parameters :");

				onlyDecorations = EditorGUILayout.Toggle("Only decorations", onlyDecorations);
				decorationPasses = (int)Mathf.Round(EditorGUILayout.Slider("Passes", decorationPasses, 1, 5));

				GUILayout.Label("Specific Generation :");

				EditorGUILayout.BeginHorizontal();
				{
					if (GUILayout.Button("Floor", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateFloors(soElementList, onlyDecorations);
						}
						SetDirtyScene();
					}

					else if (GUILayout.Button("Ceiling", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateCeilings(soElementList, onlyDecorations);
						}
						SetDirtyScene();
					}
				}
				EditorGUILayout.EndHorizontal();

				GUILayout.Label("Walls : ");

				EditorGUILayout.BeginHorizontal();
				{
					if (GUILayout.Button("All", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateWalls(soElementList, tc.LinkedIndexes, onlyDecorations);
						}
						SetDirtyScene();
					}
					else if (GUILayout.Button("North (Z+)", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateWalls(soElementList, tc.LinkedIndexes, onlyDecorations, 0);
						}
						SetDirtyScene();
					}
					else if (GUILayout.Button("East (X+)", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateWalls(soElementList, tc.LinkedIndexes, onlyDecorations, 1);
						}
						SetDirtyScene();
					}
					else if (GUILayout.Button("South (Z-)", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateWalls(soElementList, tc.LinkedIndexes, onlyDecorations, 2);
						}
						SetDirtyScene();
					}
					else if (GUILayout.Button("West (X-)", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateWalls(soElementList, tc.LinkedIndexes, onlyDecorations, 3);
						}
						SetDirtyScene();
					}
				}
				EditorGUILayout.EndHorizontal();

				GUILayout.Label("Doors : ");

				EditorGUILayout.BeginHorizontal();
				{
					if (GUILayout.Button("All", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateDoors(soElementList, tc.LinkedIndexes, onlyDecorations);
						}
						SetDirtyScene();
					}
					if (GUILayout.Button("North", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateDoors(soElementList, tc.LinkedIndexes, onlyDecorations, 0);
						}
						SetDirtyScene();
					}
					if (GUILayout.Button("East", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateDoors(soElementList, tc.LinkedIndexes, onlyDecorations, 1);
						}
						SetDirtyScene();
					}
					if (GUILayout.Button("South", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateDoors(soElementList, tc.LinkedIndexes, onlyDecorations, 2);
						}
						SetDirtyScene();
					}
					if (GUILayout.Button("West", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateDoors(soElementList, tc.LinkedIndexes, onlyDecorations, 3);
						}
						SetDirtyScene();
					}
				}
				EditorGUILayout.EndHorizontal();

				GUILayout.Label("Corners : ");

				EditorGUILayout.BeginHorizontal();
				{
					if (GUILayout.Button("All", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateCorners(soElementList, tc.LinkedIndexes, onlyDecorations);
						}
						SetDirtyScene();
					}
					else if (GUILayout.Button("NW", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateCorners(soElementList, tc.LinkedIndexes, onlyDecorations, 0);
						}
						SetDirtyScene();
					}
					else if (GUILayout.Button("NE", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateCorners(soElementList, tc.LinkedIndexes, onlyDecorations, 1);
						}
						SetDirtyScene();
					}
					else if (GUILayout.Button("SW", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateCorners(soElementList, tc.LinkedIndexes, onlyDecorations, 2);
						}
						SetDirtyScene();
					}
					else if (GUILayout.Button("SE", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) tc.GenerateCorners(soElementList, tc.LinkedIndexes, onlyDecorations, 3);
						}
						SetDirtyScene();
					}
				}
				EditorGUILayout.EndHorizontal();

				GUI.color = Color.yellow;

				GUILayout.Space(20);

				if (GUILayout.Button("Bake all tiles", GUILayout.MinHeight(30)))
				{
					BakeAllTiles();
					SetDirtyScene();
				}

				EditorGUILayout.BeginHorizontal();
				{
					if (GUILayout.Button("Set Pickable ON", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) SceneVisibilityManager.instance.EnablePicking(tc.currentCeiling.gameObject, true);
						}
						SetDirtyScene();
					}

					if (GUILayout.Button("Set Pickable OFF", GUILayout.MinHeight(30)))
					{
						foreach (Object o in Selection.objects)
						{
							GameObject go = o as GameObject;
							TileController tc = go.GetComponent<TileController>();
							if (tc) SceneVisibilityManager.instance.DisablePicking(tc.currentCeiling.gameObject, true);
						}
						SetDirtyScene();
					}
				}
				EditorGUILayout.EndHorizontal();

				GUI.color = Color.white;
			}
			EditorGUILayout.EndVertical();
		}
		EditorGUILayout.EndHorizontal();
		
		UpdateValues();
	}

	void UpdateValues()
	{
		TileController.tileSize = tileSizes;
		TileController.tileHeight = tileHeight;
		TileController.tileOffset = tileOffset;
		TileController.showGUI = showTileGUI;
		TileController.DecorationPasses = decorationPasses;
	}

	GameObject[] GetAllTileControllersObjects()
	{
		TileController[] o = FindObjectsOfType<TileController>();

		List<GameObject> r = new List<GameObject>();

		foreach (TileController tc in o)
		{
			r.Add(tc.gameObject);
		}

		return r.ToArray();
	}
	
	void BakeAllTiles()
	{
		GameObject[] tiles = GetAllTileControllersObjects();

		foreach (GameObject go in tiles)
		{
			DestroyImmediate(go.GetComponent<TileController>());
		}
	}

	void SetDirtyScene()
	{
		EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
	}

	private void OnDisable()
	{
		SceneView.duringSceneGui -= DuringSceneGUI;
	}
}
#endif