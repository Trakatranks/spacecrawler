﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ElementList : ScriptableObject
{
	[Header("Essentials")]
	public GameObject[] Walls;
	public GameObject[] Ceilings;
	public GameObject[] Floors;
	public GameObject[] Doors;
	public GameObject[] Corners;

	[Header("Decoration")]
	public GameObject[] DecoWalls;
	public GameObject[] DecoCeilings;
	public GameObject[] DecoFloors;
	public GameObject[] DecoDoors;
	public GameObject[] DecoCorners;

	[Header("FloorMaterials")]
	//1,2,3,4,corner
	public Material[] FloorMats = new Material[5];
}
