﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "Data/Level List")]
public class LevelList : ScriptableObject
{
	public LevelCategory[] Levels;
}

[System.Serializable]
public class LevelCategory
{
	public string label;
	public string[] levels;
}
