﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Player Data")]
public class PlayerData : ScriptableObject
{
	[Header("Oxygen")]
	public int BaseOxygen = 100;
	public int MaxOxygen = 100;
	public int OxygenLostPerStep = 10;

	//Move variables
	[Header("Movements")]
	public float distanceToMove = 3.0f;
	public float turnSpeed = .3f;
	public float moveSpeed = .3f;
}
