﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
	public List<Door> linkedDoors;

	public void ToggleDoors()
	{
		foreach (Door d in linkedDoors)
		{
			d.Toggle();
		}
	}
}
