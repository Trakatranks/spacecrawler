﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animation))]
public class Door : MonoBehaviour
{
	protected Animation anim;

	public bool isOpen = false;

	public AnimationClip UnlockClip;
	public AnimationClip LockClip;

	private void Start()
	{
		anim = GetComponent<Animation>();
	}

	public void Toggle()
	{
		if (isOpen)
			anim.Play(LockClip.name);
		else
			anim.Play(UnlockClip.name);
		isOpen = !isOpen;
	}
}
