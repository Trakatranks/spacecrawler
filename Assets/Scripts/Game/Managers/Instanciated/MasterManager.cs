﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// THE ONLY INSTANCIATED MANAGER
/// </summary>
public class MasterManager : MonoBehaviour
{
	public bool IsDebugMode = false;

	[Header("Game Configs")]
	public LevelList levelList;
	public PlayerData playerData;

	void Start()
    {
		GameManager.Init(playerData, levelList);
	}
}
