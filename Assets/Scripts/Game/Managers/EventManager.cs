﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void GameEvent();
public delegate void PlayerEvent(PlayerController player);

public class EventManager
{
	public static event GameEvent OnLevelInit;
	public static event PlayerEvent OnPlayerEvent;
}
