﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager
{
	public static void Init(PlayerData playerData, LevelList levelList)
	{
		SceneManager.LoadScene("Player", LoadSceneMode.Additive);
		SceneManager.LoadScene("TutoLevel", LoadSceneMode.Additive);
	}
}
