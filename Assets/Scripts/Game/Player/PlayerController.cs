﻿using InputWrapper;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	[SerializeField]
	protected PlayerData playerDataSO;

	[SerializeField]
	protected Camera PlayerCamera;
	
	protected int Oxygen = 0;

	protected bool[] AccessCards = { false, false, false };

	protected bool isMoving = false;
	protected bool isTurning = false;
	protected bool fWall = false;
	protected bool bWall = false;
	
	protected Vector2 touchStart;
	protected Vector2 touchEnd;

	private void Start()
	{
		Oxygen = playerDataSO.BaseOxygen;
	}

	// Update is called once per frame
	void Update()
    {
		HandleMobileMovement();
	}

	protected void CheckWall()
	{
		Ray fRay = new Ray(transform.position, transform.forward * 3);
		Ray bRay = new Ray(transform.position, -transform.forward * 3);

		fWall = Physics.Raycast(fRay, 3);
		bWall = Physics.Raycast(bRay, 3);
	}

	protected void HandleMobileMovement()
	{
		if (WInput.touchCount == 1)
		{
			CheckWall();

			int screenW = Screen.width;
			Touch touchInput = WInput.GetTouch(0);

			Ray inputRay = PlayerCamera.ScreenPointToRay(touchInput.position);

			if (WInput.Touches[0].phase == TouchPhase.Began && Physics.Raycast(inputRay, out RaycastHit hitInfo, 3.0f, LayerMask.GetMask("InteractiveGameObject")))
			{
				hitInfo.collider.GetComponent<Button>().ToggleDoors();
			}
			else
			{
				if (touchInput.position.x < screenW / 3)
				{
					if (!isMoving && !isTurning) StartCoroutine(Turn(-1));
				}
				else if (touchInput.position.x > 2 * screenW / 3)
				{
					if (!isMoving && !isTurning) StartCoroutine(Turn(1));
				}
				else
				{
					if (!isMoving && !isTurning && !fWall) StartCoroutine(Move(1));
				}
			}
		}
	}
	
	public void GiveOxygen(int amount)
	{
		Oxygen += amount;
		if(Oxygen > playerDataSO.MaxOxygen)
		{
			Oxygen = playerDataSO.MaxOxygen;
		}
	}

	protected void HandlePCMovement()
	{
		CheckWall();

		if (Input.GetKey(KeyCode.UpArrow))
		{
			if (!isMoving && !isTurning && !fWall)
			{
				StartCoroutine(Move(1));
			}
		}
		else if (Input.GetKey(KeyCode.DownArrow))
		{
			if (!isMoving && !isTurning && !bWall)
			{
				StartCoroutine(Move(-1));
			}
		}
		else if (Input.GetKey(KeyCode.LeftArrow))
		{
			if (!isMoving && !isTurning)
			{
				StartCoroutine(Turn(-1));
			}
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			if (!isMoving && !isTurning)
			{
				StartCoroutine(Turn(1));
			}
		}
	}

	public IEnumerator Move(int direction)
	{
		isMoving = true;

		float time = 0;

		Vector3 f = transform.forward;
		Vector3 initPos = transform.position;

		Vector3 endPos = initPos + f * playerDataSO.distanceToMove * direction;

		while (time < playerDataSO.moveSpeed)
		{
			float ratio = time / playerDataSO.moveSpeed;

			float dt = Time.deltaTime;

			time += dt;
			transform.position = Vector3.Lerp(initPos, endPos, ratio);

			yield return new WaitForEndOfFrame();
		}

		transform.position = endPos;

		isMoving = false;

		yield return null;
	}

	public IEnumerator Turn(int direction)
	{
		isTurning = true;

		float time = 0;

		Vector3 r = transform.right;
		Quaternion initRot = transform.rotation;

		Quaternion endRot = Quaternion.LookRotation(r * direction, Vector3.up);

		while (time < playerDataSO.turnSpeed)
		{
			float ratio = time / playerDataSO.turnSpeed;

			float dt = Time.deltaTime;

			time += dt;
			transform.rotation = Quaternion.Lerp(initRot, endRot, ratio);

			yield return new WaitForEndOfFrame();
		}

		transform.rotation = endRot;

		isTurning = false;

		yield return null;
	}
}
