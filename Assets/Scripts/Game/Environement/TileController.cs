﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class TileController : MonoBehaviour
{
	public static float tileSize;
	public static float tileHeight;
	public static float tileOffset;
	public static int DecorationPasses = 1;

	public static bool showLinks = false;
	public static bool showGUI = true;

	public List<TileController> LinkedTiles = new List<TileController>();

	public List<int> LinkedIndexes {
		get {
			List<int> i = new List<int>();

			foreach (TileController tc in LinkedTiles)
			{
				i.Add(GuessWallIndexFromVector(tc.transform.position));
			}

			i.Sort();

			return i;
		}
		private set { }
	}
	
	//N, E, S, W
	public Vector3[] WallAnchors = new Vector3[4];

	//NE, SE, SW, NW
	public Vector3[] CornerAnchors = new Vector3[4];

	//lists of every instanced object

	public GameObject currentCeiling;
	public GameObject currentFloor;
	public GameObject[] currentWalls = new GameObject[4];
	public GameObject[] currentDoors = new GameObject[4];
	public GameObject[] currentCorners = new GameObject[4];

	private void Update()
	{
		UpdateAnchors();
	}

	protected void UpdateAnchors()
	{
		Vector3 t = transform.position;
		float s = tileSize / 2;

		//walls
		WallAnchors[0] = t + (Vector3.forward * s);
		WallAnchors[1] = t + Vector3.right * s;
		WallAnchors[2] = t + Vector3.back * s;
		WallAnchors[3] = t + Vector3.left * s;

		//Pillars
		CornerAnchors[0] = t + new Vector3(s, 0, s);
		CornerAnchors[1] = t + new Vector3(s, 0, -s);
		CornerAnchors[2] = t + new Vector3(-s, 0, -s);
		CornerAnchors[3] = t + new Vector3(-s, 0, s);
	}

	public void GenerateTile(ElementList list)
	{
		List<int> indexes = LinkedIndexes;

		GenerateFloors(list);
		GenerateCeilings(list);
		GenerateDoors(list, indexes);
		GenerateWalls(list, indexes);
		GenerateCorners(list, indexes);

	}

	#region Generate Bases Functions

	public void GenerateCeilings(ElementList list, bool onlyDeco = false)
	{
		GameObject go = GetRandomItemFromArray(list.Ceilings);
		if (go != null)
		{
			GameObject ceiling;

			if (onlyDeco)
			{
				ceiling = currentCeiling;
				RemoveChildren(ceiling.transform);
			}
			else
			{
				DestroyImmediate(currentCeiling);
				ceiling = Instantiate(go, transform.position + Vector3.up * tileHeight, Quaternion.identity, transform);
				currentCeiling = ceiling;
			}

			GenerateDeco(list.DecoCeilings, DecorationPasses, Quaternion.identity, ceiling.transform);
		}
	}

	public void GenerateFloors(ElementList list, bool onlyDeco = false)
	{
		GameObject go = GetRandomItemFromArray(list.Floors);
		if (go != null)
		{
			GameObject floor;
			if (onlyDeco)
			{
				floor = currentFloor;
				RemoveChildren(floor.transform);
			}
			else
			{
				DestroyImmediate(currentFloor);
				floor = Instantiate(go, transform.position, Quaternion.identity, transform);

				int matIndex = GetMatIndex(LinkedIndexes.Count);
				Material newMat = list.FloorMats[matIndex];
				floor.GetComponent<MeshRenderer>().material = newMat;

				int orientationIndex = GetTileOrientationIndex(LinkedIndexes.Count);

				transform.Rotate(Vector3.up, orientationIndex * 90);

				currentFloor = floor;
			}

			GenerateDeco(list.DecoFloors, DecorationPasses, Quaternion.identity, floor.transform);
		}
	}

	public void GenerateWalls(ElementList list, List<int> indexes, bool onlyDeco = false, int specificDirection = -1)
	{
		for (int i = 0; i < 4; i++)
		{
			if (specificDirection != -1 && i != specificDirection) continue; //skips current loop if not on the specific direction

			if (!indexes.Contains(i))
			{
				GameObject go = GetRandomItemFromArray(list.Walls);
				if (go != null)
				{
					GameObject wall;

					if (onlyDeco)
					{
						wall = currentWalls[i];
						RemoveChildren(wall.transform);
					}
					else
					{
						DestroyImmediate(currentWalls[i]);
						wall = Instantiate(go, WallAnchors[i], Quaternion.Euler(0, 180 + 90 * i, 0), transform);
						currentWalls[i] = wall;
					}

					if (wall.GetComponent<Collider>() == null) wall.AddComponent<BoxCollider>();

					GenerateDeco(list.DecoWalls, DecorationPasses, Quaternion.Euler(0, 180 + 90 * i, 0), wall.transform);
				}
			}
		}
	}

	public void GenerateDoors(ElementList list, List<int> indexes, bool onlyDeco = false, int specificDirection = -1)
	{
		for (int i = 0; i < 4; i++)
		{
			if (specificDirection != -1 && i != specificDirection) continue; //skips current loop if not on the specific direction

			if (indexes.Contains(i))
			{
				GameObject go = GetRandomItemFromArray(list.Doors);
				if (go != null)
				{
					GameObject door;

					if (onlyDeco)
					{
						door = currentDoors[i];
						RemoveChildren(door.transform);
					}
					else
					{
						DestroyImmediate(currentDoors[i]);
						door = Instantiate(go, WallAnchors[i], Quaternion.Euler(0, 180 + 90 * i, 0), transform);
						currentDoors[i] = door;
					}

					GenerateDeco(list.DecoDoors, DecorationPasses, Quaternion.Euler(0, 180 + 90 * i, 0), door.transform);
				}
			}
		}
	}

	public void GenerateCorners(ElementList list, List<int> indexes, bool onlyDeco = false, int specificDirection = -1)
	{
		for (int i = 0; i < 4; i++)
		{
			if (specificDirection != -1 && i != specificDirection) continue; //skips current loop if not on the specific direction

			if (indexes.Contains(i) && indexes.Contains((i + 1) % 4)) //si la tile présente un virage
			{
				GameObject go = GetRandomItemFromArray(list.Corners);
				if (go != null)
				{
					GameObject corner;

					if (onlyDeco)
					{
						corner = currentCorners[i];
						RemoveChildren(corner.transform);
					}
					else
					{
						DestroyImmediate(currentCorners[i]);

						corner = Instantiate(go, CornerAnchors[i], Quaternion.Euler(0, 270 + 90 * i, 0), transform);
						currentCorners[i] = corner;
					}

					GenerateDeco(list.DecoCorners, DecorationPasses, Quaternion.Euler(0, 270 + 90 * i, 0), corner.transform);
				}
			}
		}
	}

	#endregion
	
	protected void GenerateDeco(GameObject[] objectList, int passes, Quaternion rotation, Transform parent = null)
	{
		Transform par = parent == null ? transform : parent.transform;
		
		for (int p = 0; p < passes; p++)
		{
			GameObject go = GetRandomItemFromArray(objectList);

			if (go != null) Instantiate(go, par.position, rotation, parent);
		}
	}

	public void RemoveChildren(Transform t)
	{
		if (t.childCount != 0)
		{
			int c = t.childCount;

			for (int i = c - 1; i >= 0; i--)
			{
				if (t.GetChild(i).gameObject != null)
					DestroyImmediate(t.GetChild(i).gameObject);
			}
		}
	}

	protected GameObject GetRandomItemFromArray(GameObject[] array)
	{
		if (array?.Length != 0)
		{
			int l = array.Length;
			int index = (int)Mathf.Floor(Random.Range(0, l));
			return array[index];
		}
		return null;
	}

	/// <summary>
	/// Returns -1 if no values have been found
	/// </summary>
	/// <param name="direction"></param>
	/// <param name="guessEstimation"></param>
	/// <returns></returns>
	public int GuessWallIndexFromVector(Vector3 direction, float guessEstimation = .9f)
	{
		Vector3 directionVector = (direction - transform.position).normalized;

		for (int i = 0; i < 4; i++)
		{
			Vector3 refVector = (WallAnchors[i] - transform.position).normalized;

			if (Vector3.Dot(directionVector, refVector) >= guessEstimation) return i;
		}

		return -1;
	}

	public int GetMatIndex(int linkAmount)
	{
		if (linkAmount != 2) return linkAmount - 1;
		else
		{
			if (LinkedIndexes[1] - LinkedIndexes[0] == 2) return 1;
			else
			{
				//Debug.Log(LinkedIndexes[1] - LinkedIndexes[0]);
				return 4;
			}
		}
	}

	public int GetTileOrientationIndex(int linkAmount)
	{
		int result = -1;
		switch (linkAmount)
		{
			case 1:
				result = LinkedIndexes[0];
				break;
			case 2:
				if (LinkedIndexes[1] - LinkedIndexes[0] == 2) result = LinkedIndexes[0];
				else
				{
					if(LinkedIndexes[1] - LinkedIndexes[0] == 1) result = LinkedIndexes[0];
					else result = LinkedIndexes[1];
				}
				break;
			case 3:
				int sum = 0;
				foreach (int i in LinkedIndexes)
				{
					sum += i;
				}
				result = (-sum + 7) % 4;
				break;
			case 4:
				result = 0;
				break;
			default:
				break;
		}
		return result;
	}

	public static void Connect(TileController a, TileController b)
	{
		Vector3 aPos = a.transform.position;
		Vector3 bPos = b.transform.position;

		if (Vector3.Distance(aPos, bPos) > tileSize * 1.4) return;

		if (!a.LinkedTiles.Contains(b)) a.LinkedTiles.Add(b);
		if (!b.LinkedTiles.Contains(a)) b.LinkedTiles.Add(a);
	}

	public static void Disconnect(TileController a, TileController b)
	{
		if (a.LinkedTiles.Contains(b)) a.LinkedTiles.Remove(b);
		if (b.LinkedTiles.Contains(a)) b.LinkedTiles.Remove(a);
	}

	private void OnDrawGizmos()
	{
		float ts = tileSize - tileOffset / 2;

		float S2 = ts / 2;

		Vector3 t = transform.position;

		if (showLinks)
		{
			//draw center
			Gizmos.color = Color.green;

			Gizmos.DrawCube(t, Vector3.one * .25f);

			foreach (TileController tc in LinkedTiles)
			{
				if (tc == null) continue;
				Gizmos.DrawLine(t, t + (tc.transform.position - t) / 2);
			}

			Gizmos.color = Color.white;
		}

		if (showGUI)
		{
			DrawFlatRect(t, tileSize, tileSize);


			for (int i = -1; i <= 1; i++)
			{
				Vector3 posleft = t + new Vector3(-S2, 0, S2 * i);
				Gizmos.DrawSphere(posleft, .1f);

				Vector3 posright = t + new Vector3(S2, 0, S2 * i);
				Gizmos.DrawSphere(posright, .1f);

				if (i != 0)
				{
					Vector3 poscenter = t + new Vector3(0, 0, S2 * i);
					Gizmos.DrawSphere(poscenter, .1f);
				}
			}
		}
	}

	private void DrawFlatRect(Vector3 center, float x, float z)
	{
		Vector3 pos = center;
		float sx = x / 2;
		float sz = z / 2;

		Vector3 TL = pos + new Vector3(-sx, 0, sz);
		Vector3 TR = pos + new Vector3(sx, 0, sz);
		Vector3 BR = pos + new Vector3(sx, 0, -sz);
		Vector3 BL = pos + new Vector3(-sx, 0, -sz);

		Gizmos.DrawLine(TL, TR);
		Gizmos.DrawLine(TR, BR);
		Gizmos.DrawLine(BR, BL);
		Gizmos.DrawLine(BL, TL);
	}

}
